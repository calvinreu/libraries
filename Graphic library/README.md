# Graphic module

## TODO

add tests for all functions in test file

## functions

### init
 > void init(const vector<string> &path_to_images, const int &screenheight, const int &screenwidth, const bool &fullscreen, const char* window_name, const bool &init_SDL)

#### path_to_images
the argument path to images is a vector of strings, which contain the filenames of the images. The path_to_images argument also contains information about [renderimage_number](#renderimage_number)

#### screenheight 
screenheight defines the hight in pixelnumber.

#### screenwidth
screenwidth defines the width in pixelnumber.

#### fullscreen
if fullscreen is true the window will be fullscreen. **!THIS FEATURE IS NOT WORKING AT THE MOMENT**

#### windowname
windowname is a char* to the name of the window

#### init_SDL
if init_SDL is true it will only init SDL_VIDEO if you want to init more than only SDL_VIDEO init SDL before calling the init function of visual SDL_VIDEO must be initialized

### new_frame
 > void new_frame()

this function renders a new frame to the window

### remove_object
 > void remove_object(const Uint32* ID)

#### ID
The required Uint32* is the [ID](#return-value-id)

### add_object
 > Uint32* add_object(const size_t &renderimage_number, const SDL_Rect &srcrect, const SDL_Rect &drect)

#### renderimage_number
The renderimage number is the location of the texture the new object will have. The order of strings in the vector, which is used for the [init](#init) function, is the order of images. For example if the renderimage_number is 2 it will load the texture, which was loaded with the string at place 2.

#### srcrect
srcrect is a rectangle used to load a certain part of the texture. If you want the whole image x and y of the rectangle are 0, h is the hight of the image and w is the width of the image.

#### drect
drect is the destination rectangle it describes the position(drect.x && drec.y), hight(h) and width(w). I wouldn't recommend using a other ratio then the srcrect.

#### return value (ID)
the pointer to the information where the object is.

### change_postition
 > void change_postition(const Uint32* &ID, const size_t &xpos, const size_t &ypos); 

#### ID
The required Uint32* is the [ID](#return-value-id)

#### xpos
xpos is add to the x of the [drect](#drect) this way the object changes position on x axis

#### ypos
ypos is add to the y of the [drect](#drect) this way the object changes position on y axis
