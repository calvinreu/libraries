#include "window.hpp"

visual::~visual()
{
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);

    for(auto i = sprites.begin(); i < sprites.end(); i++){
        SDL_FreeSurface(i->texture);
        for (auto i0 = i->instances.begin(); i0 < i->instances.end(); i0++)
            SDL_DestroyTexture(i0->sprite);
        
    }

    SDL_DestroyRenderer( renderer );
    SDL_DestroyWindow( window );
    window = NULL;
    renderer = NULL;


    SDL_Quit();
    IMG_Quit();
}

void visual::init(const char** path_to_images, const size_t imgC, int screenheight, int screenwidth, const bool &fullscreen, const char* window_name, const bool &init_SDL)
{
    if(init_SDL)
        SDL_Init(SDL_INIT_VIDEO);

    window = SDL_CreateWindow( window_name, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenwidth, screenheight, SDL_WINDOW_SHOWN);

    if(window == NULL)
        std::cout << "Window could not be created! SDL Error: %s\n" << SDL_GetError() << std::endl;
  
    renderer = SDL_CreateRenderer( window, -1, 0 );

    if( renderer == NULL )
        std::cout << "Renderer could not be initialize! SDL Error: %s\n" << SDL_GetError() << std::endl;

    int imgFlags = IMG_INIT_PNG;

    if( !( IMG_Init( imgFlags ) & imgFlags ) )
        std::cout << "SDL_image could not be initialize! SDL Error: %s\n" << SDL_GetError() << std::endl;

    SDL_Surface* loadedSurface = NULL;

    for (size_t i = 0; i < imgC; i++)
    {
        loadedSurface = IMG_Load(path_to_images[i]);

        if( loadedSurface == NULL )
            std::cout << "unable to load image: %s\n" << path_to_images[i] << IMG_GetError() << std::endl;
    
        
        sprites.push_back(INTERNAL::sprite(loadedSurface));

        if(sprites.back().texture == NULL )
            std::cout << "Unable to create sprites from %s! SDL Error: %s\n" << path_to_images[i] << SDL_GetError() << std::endl;
    }

    freeSpace.assign(imgC, std::queue<INTERNAL::ID>());
    
}

void visual::new_frame()
{

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    for (auto i = sprites.begin(); i < sprites.end(); i++)
        for (auto i1 = i->instances.begin(); i1 < i->instances.end(); i1++)
            if(i1->visible)
                SDL_RenderCopy( renderer, i1->sprite, &(i1->srcrect), &(i1->desrect));

    SDL_RenderPresent(renderer);
}

void visual::remove_object(const INTERNAL::ID &ID)
{
    if(ID.instanceC == sprites[ID.spriteC].instances.size() - 1){
        SDL_DestroyTexture(sprites[ID.spriteC].instances.back().sprite);
        sprites[ID.spriteC].instances.pop_back();
    }
    else
    {
        SDL_DestroyTexture(sprites[ID.spriteC].instances.back().sprite);
        freeSpace[ID.spriteC].push(ID);
        sprites[ID.spriteC].instances[ID.instanceC].visible = false;
    }
    
}

INTERNAL::ID visual::add_object(const size_t &sprite_number, const SDL_Rect &srcrect, const SDL_Rect &drect)
{
    INTERNAL::ID temp_ID;
    INTERNAL::instance temp_object_info(sprites[sprite_number].texture, renderer);

    temp_object_info.srcrect = srcrect;
    temp_object_info.desrect = drect;

    if(freeSpace[sprite_number].size() > 0)
    {
        temp_ID = freeSpace[sprite_number].front();
        freeSpace[sprite_number].pop();
        sprites[sprite_number].instances[temp_ID.instanceC] = temp_object_info;
    }else{
        temp_ID.spriteC = sprite_number;
        temp_ID.instanceC = sprites[sprite_number].instances.size();
        sprites[sprite_number].instances.push_back(temp_object_info);
    }

    return temp_ID;
}

void visual::change_postition(const INTERNAL::ID &ID, const int &xpos, const int &ypos)
{

    SDL_Rect* rectToChange = &(sprites[ID.spriteC].instances[ID.instanceC].desrect);

    rectToChange->x += xpos;
    rectToChange->y += ypos;

}

void visual::change_size(const INTERNAL::ID &ID, const int &high, const int &width)
{

    SDL_Rect* rectToChange = &(sprites[ID.spriteC].instances[ID.instanceC].desrect);
    
    rectToChange->w += width;
    rectToChange->h += high;
}

void visual::set_position(const INTERNAL::ID &ID, const int &xpos, const int &ypos)
{

    SDL_Rect* rectToChange = &(sprites[ID.spriteC].instances[ID.instanceC].desrect);

    rectToChange->x = xpos;
    rectToChange->y = ypos;
}

void visual::set_size(const INTERNAL::ID &ID, const int &high, const int &width)
{

    SDL_Rect* rectToChange = &(sprites[ID.spriteC].instances[ID.instanceC].desrect);
    
    rectToChange->w = width;
    rectToChange->h = high;
}

void visual::scale_object(const INTERNAL::ID &ID, const double &scale_factor)
{

    SDL_Rect* rectToChange = &(sprites[ID.spriteC].instances[ID.instanceC].desrect);
    
    rectToChange->w *= scale_factor;
    rectToChange->h *= scale_factor;

}

SDL_Rect& visual::operator[](const INTERNAL::ID &ID)
{
    return (sprites[ID.spriteC].instances[ID.instanceC].desrect);
}

SDL_Rect& visual::get_srcrect(const INTERNAL::ID &ID)
{
    return (sprites[ID.spriteC].instances[ID.instanceC].srcrect);
}