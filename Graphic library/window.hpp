#pragma once
#include <vector>
#include <queue>
#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

namespace INTERNAL{

union ID
{
    struct{ unsigned int spriteC, instanceC; };
    unsigned long int ID;
};

struct instance
{
    bool visible;
    SDL_Rect srcrect;
    SDL_Rect desrect;
    SDL_Texture* sprite;
    instance(SDL_Surface* surface, SDL_Renderer* renderer) : sprite(SDL_CreateTextureFromSurface(renderer, surface)), visible(true){}
    instance() : visible(true) {}
    
};


struct sprite
{
    SDL_Surface* texture  = NULL;
    std::vector<INTERNAL::instance> instances;

    sprite(SDL_Surface* texture) : texture(texture){}
};

}

class visual
{
private:
    std::vector<INTERNAL::sprite> sprites;
    std::vector<std::queue<INTERNAL::ID>> freeSpace;
    SDL_Window*   window   = NULL;
    SDL_Renderer* renderer = NULL;
public:

    visual(){}
    ~visual();

    void init(const char** path_to_images, const size_t imgC, 
        int screenheight, 
        int screenwidth, 
        const bool &fullscreen, 
        const char* window_name, 
        const bool &init_SDL);

    void new_frame();
    void remove_object(const INTERNAL::ID &ID);
    INTERNAL::ID add_object(const size_t &sprite_number, const SDL_Rect &srcrect, const SDL_Rect &drect);
    void change_postition(const INTERNAL::ID &ID, const int &xpos, const int &ypos);
    void change_size(const INTERNAL::ID &ID, const int &high, const int &width);
    void set_position(const INTERNAL::ID &ID, const int &xpos, const int &ypos);
    void set_size(const INTERNAL::ID &ID, const int &high, const int &width);
    void scale_object(const INTERNAL::ID &ID, const double &scale_factor);

    SDL_Rect& operator[](const INTERNAL::ID &ID);
    SDL_Rect& get_srcrect(const INTERNAL::ID &ID);

};