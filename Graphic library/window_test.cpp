#include "window.hpp"

const SDL_Rect srcrect = {.x = 0, .y = 0, .w = 34, .h = 101};

typedef INTERNAL::ID rectangle;


int main()
{
    visual testobject;
    std::vector<std::string> testIMG;
    std::vector<std::vector<rectangle>> rectangles;

    for(size_t i = 0; i < 10; i++)
        testIMG.push_back("./rectangle.png");

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER);

    testobject.init(testIMG, 1000, 1000, false, "test window", false);

    SDL_Rect rectangle_drect;
    rectangle* last_element;

    std::cout << "adding objects" << std::endl;

    for(size_t i = 0; i < 10; i++)
    {
        rectangles.push_back(std::vector<rectangle>());
        for (size_t i1 = 0; i1 < 10; i1++)
        {
            rectangles.back().push_back(rectangle());
            rectangle_drect = {.x = i*50, .y = i1*50, .w = i+1, .h = i1+1};
            rectangles.back().back() = testobject.add_object(i, srcrect, rectangle_drect);
        }
    }

    std::cout << "IDs: " << std::endl;

    for (auto i = rectangles.begin(); i < rectangles.end(); i++)
        for (auto i0 = i->begin(); i0 < i->end(); i0++)
            std::cout << i0->ID << std::endl;
        
    std::cout << std::endl;
    std::cout << "objects will be changed" << std::endl;

    for(size_t i = 0; i < 300; i++){
        testobject.new_frame();

        for(auto i0 = rectangles.begin(); i0 < rectangles.end(); i0++)
            for (auto i1 = i0->begin(); i1 < i0->end(); i1++){
                //std::cout << "ID = " << i1->IMG_ID.instanceC << "  " << i1->IMG_ID.spriteC << std::endl;
                testobject.change_postition(*i1, 1, 1);
            }
    }

    for(size_t i = 0; i < 300; i++){
        testobject.new_frame();

        for(auto i0 = rectangles.begin(); i0 < rectangles.end(); i0++)
            for (auto i1 = i0->begin(); i1 < i0->end(); i1++){
                //std::cout << "ID = " << i1->IMG_ID.instanceC << "  " << i1->IMG_ID.spriteC << std::endl;
                testobject.change_postition(*i1, 0-1, 0-1);
            }
    }
}