#include <iostream>
#include <thread>
#include <string>


inline auto start_command(const std::string &command)
{
    std::thread commandthread(system, command);
    return commandthread.get_id();

}

inline auto start_extension(
    const char*  &extensionfile,
    const char** &arguments    ,
    const int    &argumentcount)
{
    std::string command;

    command.append(std::string(extensionfile).append(""));

    for (int i = 1; i < argumentcount; i++)
        command.append(std::string(arguments[i]).append(" "));

    return start_command(command);
}