typedef unsigned long int size_t;

template<typename T>
struct matrix
{

    T** m_matrix;
    size_t* m_lengts;
    size_t m_dimmensions;

    matrix(m_matrix, m_lengts, m_dimmensions) 
    : m_matrix(m_matrix), m_lengts(m_lengts), m_dimmensions(m_dimmensions){}
    matrix(){}
    ~matrix();

    T& operator[](size_t){return m_matrix[size_t];}
};