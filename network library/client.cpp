#include "client.hpp"

void client::init(const char* serverip, const Uint16 &port)
{
    SDLNet_Init();
    SDLNet_ResolveHost(&ip, serverip, port);

    server = SDLNet_TCP_Open(&ip);
}

client::~client()
{
    SDLNet_Quit();
    SDLNet_TCP_Close(server);
}