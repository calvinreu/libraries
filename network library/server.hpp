#pragma once
#include <SDL2/SDL_net.h>
#include <vector>


class server
{
private:
    IPaddress ip;
    std::vector<TCPsocket> clients;
    TCPsocket serversocket;

public:
    server(const size_t &port);
    ~server();

    void new_client(){ clients.push_back(SDLNet_TCP_Accept(serversocket)); }
    void send_msg_to_client(const void* data, const size_t &msg_size, const size_t &client){ SDLNet_TCP_Send(clients[client], data, msg_size); }
    void send_msg_to_all(const void* data, const size_t &msg_size);
    void recive_msg(void* buffer, const size_t &msg_size, const size_t &client){ SDLNet_TCP_Recv(clients[client], buffer, msg_size); }
};