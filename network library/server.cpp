#include "server.hpp"

server::server(const size_t &port)
{
    SDLNet_Init();
    SDLNet_ResolveHost(&ip, NULL, port);

    serversocket = SDLNet_TCP_Open(&ip);

}

server::~server()
{
    SDLNet_Quit();

    for (auto i = clients.begin(); i < clients.end(); i++)
        SDLNet_TCP_Close(*i);
    
}

void server::send_msg_to_all(const void* data, const size_t &msg_size)
{
    for (auto i = clients.begin(); i < clients.end(); i++)
        SDLNet_TCP_Send(*i, data, msg_size);
    
}