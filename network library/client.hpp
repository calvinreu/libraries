#include <SDL2/SDL_net.h>
#include <string>


class client
{
private:
    TCPsocket server;
    IPaddress ip;
public:
    client();
    ~client();

    void init(const char* serverip, const Uint16 &port);
    void send_msg(const void* data, const size_t &msg_size) const { SDLNet_TCP_Send(server, data, msg_size); }
    void recive_msg(void* buffer, const size_t &msg_size) const { SDLNet_TCP_Recv(server, buffer, msg_size); }

};
